# Javascript Training

My name his M. Hassaan Zulqarnain. The purpose of this training is to 
learn different concepts of javascript as follows:

Grammar and types
Control flow and error handling
Loops and iteration
Functions
Expressions and operators
Numbers and dates
Text formatting
Regular expressions
Indexed collections
Keyed collections
Working with objects
Using classes